<?php
// add new Fields to Sites-Konfiguration

call_user_func(
	static function () {
		$newFields = [
			'nabu_template_options' => [
				'label' => 'Layout',
				'config' => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
						['Allgemein', '--div--'],
						['NABU Template', 'nabu-template'],
						['NABU Template mit Mega Menü', 'nabu-mega'],
						['NAJU Template', 'naju-template'],
						['NAJU Template mit Mega Menü', 'naju-template-mega'],
					],
					'size' => 6,
				],
			],
			'newslist' => [
				'label' => 'ID der Aktuelles Seite',
				'config' => [
					'type' => 'input',
					'default' => '3'
				]
			],
			'headNavigation' => [
				'label' => 'ID der Kopfnavigation',
				'config' => [
					'type' => 'input',
					'default' => '17'
				]
			],
			'searchResult' => [
				'label' => 'ID der Suchergebnisseite',
				'config' => [
					'type' => 'input',
					'default' => '23'
				]
			],
			'logo' => [
				'label' => 'Pfad zum Logo',
				'config' => [
					'type' => 'input',
					'default' => '/fileadmin/user_upload/Images/logo_einzeilig.jpg'
				]
			],
			'Facebook' => [
				'label' => 'Facebook Seiten-Link',
				'config' => [
					'type' => 'input',
					'default' => 'http://www.facebook.com/Naturschutzbund'
				]
			],
			'Twitter' => [
				'label' => 'Twitter Seiten-Link',
				'config' => [
					'type' => 'input',
					'default' => 'http://www.twitter.com/nabu_de'
				]
			],
			'Mail' => [
				'label' => 'E-Mail Fusszeile',
				'config' => [
					'type' => 'input',
					'default' => 'nabu@nabu.de'
				]
			],
			'Instagram' => [
				'label' => 'Instagram Seiten-Link',
				'config' => [
					'type' => 'input',
					'default' => 'https://www.instagram.com/nabu/'
				]
			],
			'YouTube' => [
				'label' => 'YouTube Seiten-Link',
				'config' => [
					'type' => 'input',
					'default' => 'https://www.youtube.com/channel/UCu4rogZzFsl9KyJDy4opROQ'
				]
			]
		];
		foreach ($newFields as $key => $value) {
			$GLOBALS['SiteConfiguration']['site']['columns'][$key] = $value;
		}

		$GLOBALS['SiteConfiguration']['site']['types']['0']['showitem'] .= ',--div--;NABU Template,' . implode(',', array_keys($newFields));
	}
);
