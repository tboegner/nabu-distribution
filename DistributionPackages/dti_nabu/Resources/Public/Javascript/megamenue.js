// JS für Mega Menue

+(function ($) {

	"use strict";

	// initialize
	var $window = $(window);
	var $document = $(document);
	var open = false;
	var breakpointSmallDevices = 500; // has to be 768 in production! Should match the bootstrap breakpoint value
	var toggle = '[data-toggle=dropdown]';
	var bsEventName = 'click.bs.dropdown.data-api';
	var $navbar = $('.navbar');
	var bsDropdown = $.fn.dropdown.Constructor;
	var bsClickHandler = bsDropdown.prototype.toggle;

	// events
	$navbar.on('touchstart click mouseenter mouseleave', '.submenu', dropdownHandler);

	/**
	 * dropdown event handler
	 *
	 * @param evt
	 * @returns {boolean}
	 */
	function dropdownHandler(evt) {
		var isMobile = parseInt($window.width()) < breakpointSmallDevices;

		// default behavior on mobile
		if (isMobile) {
			return true;
		}

		// remove focus
		if (evt.type === 'mouseleave') {
			$navbar.find(':focus').blur();
		}

		// trigger link
		var $target = $(evt.target);

		if ($target.attr('href') && ((open && evt.type === 'click') || evt.type === 'touchstart')) {
			window.location.assign($target.attr('href'));

			return true;
		}

		// prevent default behavior and update status
		evt.preventDefault();
		open = evt.type === 'mouseenter';

		// trigger bootstrap event
		$(this).find('[data-toggle=dropdown]').trigger(bsEventName);

		// switch event handling
		if (open) {
			$document.off(bsEventName, toggle, bsClickHandler);
		} else {
			$document.on(bsEventName, toggle, bsClickHandler);
		}

		return false;
	}
})(window.jQuery);
