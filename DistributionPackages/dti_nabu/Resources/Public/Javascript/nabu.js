$( document ).ready(function() {

    /////////////////////////////////
	// Dreieck fuer die Karten
    /////////////////////////////////   
    $('.card:has(.card-img-top)').addClass('card-triangle');


    /////////////////////////////////
    // Hauptnavigation auf volle Breite bringen
    /////////////////////////////////

    var $MainNavElements = $('.navbar .hidden-md-down .nav .nav-item');
    var NavCount = $MainNavElements.length;
    var MainNavElementsWidth = 100/NavCount;
    $MainNavElements.css('text-align', 'center').css('margin', 0).css('border-left', '1px solid #fff').css('width', MainNavElementsWidth+'%');


    /////////////////////////////////
    // Mobiles Menu auf Unterseiten
    /////////////////////////////////

    // Inhalte des Menus reinkopieren
    $('#mobilenavleft').html($('.nav-main-right').html());

    // Menu Position in der vertikalen bestimmen
    var topposition = $('a.mobilenavleft').offset().top;
    //var left = $('#mobilenavleft').offset().left;
    $('#mobilenavleft').css('top', topposition);
    console.log(topposition);


    // Menu einblenden
    $('a.mobilenavleft').click(function(){
        $('body').toggleClass('mobilenavleft-open');
    });


    /////////////////////////////////
    // Mobiles Menu auf Unterseiten
    /////////////////////////////////

    var slickArrow = $();

    $('.slick').slick({
        //setting-name: setting-value
        slidesToShow: 3,
        slidesToScroll: 1,
        nextArrow: $('#slickright'),
        prevArrow: $('#slickleft'),
        speed: 200,
        responsive: [
            {
                breakpoint: 751,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 750,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
        ]

    });
});

$(".nabu-overlay").hover(function () {
    $(".nabu-overlay").toggleClass("result_hover");
});

$("#stoerer_closebutton").click(function () {
    $(".nabu-overlay").hide();
});

$(document).ready(function(){
    $('body .carousel-item:first').addClass('active');
});