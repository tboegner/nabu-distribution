<?php
namespace DTInternet\DtiNabu\Controller;
/*
 *
 * Copyright (c) 2016 D&T Internet GmbH bublies@dt-internet.de
 *
 * Author Stefan Bublies, bublies@dt-internet.de
 * @link     http://www.dt-internet.de
 * @package  NABU TYPO3 CMS Vorlage
 *
 */


/**
 * Controller: Slideshow
 *
 * @package DTInternet\DtiNabu\Controller
 */

use TYPO3\CMS\Core\Utility\DebugUtility;

class DtiNabuSlideshowController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \DTInternet\DtiNabu\Domain\Repository\SlideRepository
     *
     */
    protected $slideRepository;

		/**
		 * @param \DTInternet\DtiNabu\Domain\Repository\SlideRepository|null $slideRepository
		 */
		public function __construct(?\DTInternet\DtiNabu\Domain\Repository\SlideRepository $slideRepository)
		{
			$this->slideRepository = $slideRepository;
		}


	/**
     * Zeige die Slides in der Slideshow
     *
     * @return void
     */
    public function showAction()
    {
        $currentId = $this->configurationManager->getContentObject()->data;
        $this->view->assignMultiple(array(
            'identifier' => 'dti-nabu-carousel-id-' . $currentId['uid'],
            'slides' => $this->slideRepository->findByContentId($currentId['uid']),
        ));
    }
}
